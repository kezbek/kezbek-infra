# Kezbek Infra
This repository is to collect all related infra config to kezbek main service app

* * *

## terraform
this folder contains terraform config and script to build cloud enviroment to aws cloud provider.

### Prerequsite
- [Terraform](https://www.terraform.io/)

### How to apply
1. ```cd terraform```
2. set your aws credentials to ~/.aws/credentials
3. adjust your db_password to your needs in modules/environtment/{env}/rds-{env}.tf
4. run this command
```bash
terraform init
terraform validate # to validate terraform file
terraform plan -out plan.out
terraform apply "plan.out" # to apply changes in terraform file

terraform destroy # to destroy changes
```