provider "aws" {
  region = "us-east-1"
  profile = "kezbek-engineer"
 ## access_key = $AWS_ACCESS_KEY_ID
 ## secret_key = $AWS_SECRET_ACCESS_KEY
}

module "networking" {
  source    = "./modules/network"
  project = "KEZBEK"
  owner   = "kezbek-eng"

  cidr_block                 = "10.0.0.0/16"
  private_subnet_cidr_blocks = ["10.0.1.0/24", "10.0.3.0/24"]
  public_subnet_cidr_blocks  = ["10.0.2.0/24", "10.0.4.0/24"]
  environment                = "Prod-Dev"
}

 module "dev" {
   source      = "./modules/environment/dev"
   depends_on  = [module.networking]
   vpc         = module.networking.vpc
   sg          = module.networking.sg
   sub_pvt     = module.networking.private_subnet_ids
   sub_pub     = module.networking.public_subnet_ids
   sub_db      = module.networking.db_subnet_ids
   project     = "KEZBEK"
   owner       = "kezbek-eng"
   environment = "Dev"
}
