########################
# RDS for DEV
########################
resource "aws_db_parameter_group" "kezbek_db_dev" {
  name   = "kezbek"
  family = "postgres14"

  parameter {
    name  = "log_connections"
    value = "1"
  }
}

resource "aws_db_instance" "kezbek_db_dev" {
  db_subnet_group_name = var.sub_db.name
  allocated_storage = 20
  identifier = "kezbek-db-dev"
  storage_type = "gp2"
  engine = "postgres"
  engine_version = "14.1"
  instance_class = var.instance_db_dev
  username = "kezbekadmin"
  password = "xxxxxxxxxxx"
  parameter_group_name = aws_db_parameter_group.kezbek_db_dev.name
  publicly_accessible = true
  skip_final_snapshot = true
}
