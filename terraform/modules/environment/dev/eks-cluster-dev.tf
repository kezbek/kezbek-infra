##################
# EKS Cluster
##################
resource "aws_iam_role" "kezbek-cluster-dev" {
  name = "kezbek-cluster-dev"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::646915826882:user/engineer",
        "Service": "eks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "kezbek-cluster-dev-AmazonEKSClusterPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = "${aws_iam_role.kezbek-cluster-dev.name}"
}

resource "aws_iam_role_policy_attachment" "kezbek-cluster-dev-AmazonEKSServicePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSServicePolicy"
  role       = "${aws_iam_role.kezbek-cluster-dev.name}"
}

resource "aws_iam_role_policy_attachment" "kezbek-cluster-dev-AmazonEC2ContainerRegistryReadOnly" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = "${aws_iam_role.kezbek-cluster-dev.name}"
}

resource "aws_iam_role_policy_attachment" "kezbek-cluster-dev-AmazonEKSWorkerNodePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = "${aws_iam_role.kezbek-cluster-dev.name}"
}

resource "aws_security_group" "kezbek-cluster-dev" {
  name        = "kezbek-cluster-dev"
  description = "Cluster communication with worker nodes"
  vpc_id      = "${var.vpc.id}"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = merge(
   {
     Name        = "kezbek-cluster-dev"
     Project     = var.project,
     Owner       = var.owner,
     Environment = "Development"
   },
   var.tags
   )
}

resource "aws_eks_cluster" "kezbek-cluster-dev" {
  name     = "${var.cluster-name}-dev"
  role_arn = "${aws_iam_role.kezbek-cluster-dev.arn}"

  vpc_config {
    security_group_ids = ["${aws_security_group.kezbek-cluster-dev.id}"]
    subnet_ids         = var.sub_pvt.*.id
  }

  depends_on = [
    aws_iam_role_policy_attachment.kezbek-cluster-dev-AmazonEKSClusterPolicy,
    aws_iam_role_policy_attachment.kezbek-cluster-dev-AmazonEKSServicePolicy,
    var.sub_pub,
  ]

  tags = merge(
    {
      "k8s.io/cluster-autoscaler/kezbek-cluster-dev"         = "owned",
      "k8s.io/cluster-autoscaler/enabled"                 = "true"
    }
    )
}
