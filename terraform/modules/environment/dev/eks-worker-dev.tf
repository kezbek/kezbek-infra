####################
# EKS Worker Node
####################
module "kezbek-node-dev" {
  //count   = length(var.sub_pvt.*.id)
  source  = "umotif-public/eks-node-group/aws"
  version = "~> 4.0.0"

  cluster_name = aws_eks_cluster.kezbek-cluster-dev.id

  node_group_name_prefix = "kezbek-node-dev-"

  subnet_ids = var.sub_pvt.*.id

  desired_size = 2
  min_size     = 1
  max_size     = 2

  instance_types = var.node_dev
  capacity_type  = "ON_DEMAND"

  ec2_ssh_key = "key-11"

  labels = {
    lifecycle = "OnDemand"
  }

  force_update_version = true

  tags = merge(
   {
     Project     = var.project,
     Owner       = var.owner,
     Environment = "Development",
     "k8s.io/cluster-autoscaler/kezbek-cluster-dev" = "owned",
     "k8s.io/cluster-autoscaler/enabled" = "true"
   },
   var.tags
   )
}
