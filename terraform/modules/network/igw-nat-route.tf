#################
# Internet GW
#################
resource "aws_internet_gateway" "main" {
  vpc_id = aws_vpc.main.id

  tags = merge(
    {
      Name        = "Kezbek-Internet-Gateway",
      Project     = var.project,
      Owner       = var.owner,
      Environment = var.environment
    },
    var.tags
  )
}

#################
# NAT GW
#################
resource "aws_eip" "nat" {
  vpc = true
}

resource "aws_nat_gateway" "main" {
  allocation_id     = aws_eip.nat.id
  subnet_id         = aws_subnet.public[0].id

  tags = merge(
    {
      Name        = "Kezbek-NAT-Gateway",
      Project     = var.project,
      Owner       = var.owner,
      Environment = var.environment
    },
    var.tags
  )
}

#################
# RT Public
#################
resource "aws_route_table" "public" {
  count = length(var.private_subnet_cidr_blocks)
  vpc_id = aws_vpc.main.id

  tags = merge(
    {
      Name        = "Kezbek-Route-Table-Public",
      Project     = var.project,
      Owner       = var.owner,
      Environment = var.environment
    },
    var.tags
  )
}

#################
# RTA Public
#################
resource "aws_route_table_association" "rta_public" {
  count = length(var.private_subnet_cidr_blocks)
  subnet_id      = aws_subnet.public[count.index].id
  route_table_id = aws_route_table.public[count.index].id
}

#################
# Route Public
#################
resource "aws_route" "public" {
  count = length(var.private_subnet_cidr_blocks)
  route_table_id         = aws_route_table.public[count.index].id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.main.id
}

#################
# RT Private
#################
resource "aws_route_table" "private" {
  count = length(var.private_subnet_cidr_blocks)

  vpc_id = aws_vpc.main.id

  tags = merge(
    {
      Name        = "Kezbek-Route-Table-Private",
      Project     = var.project,
      Owner       = var.owner,
      Environment = var.environment
    },
    var.tags
  )
}

#################
# RTA Private
#################
resource "aws_route_table_association" "rta_private" {
  count = length(var.private_subnet_cidr_blocks)

  subnet_id      = aws_subnet.private[count.index].id
  route_table_id = aws_route_table.private[count.index].id
}

#################
# Route Private
#################
resource "aws_route" "private" {
  count = length(var.private_subnet_cidr_blocks)

  route_table_id         = aws_route_table.private[count.index].id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.main.id
}
