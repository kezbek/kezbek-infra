data "aws_availability_zones" "available" {
  state = "available"
}
#################
# Public Subnet
#################
resource "aws_subnet" "public" {
  count                   = length(var.public_subnet_cidr_blocks)
  vpc_id                  = aws_vpc.main.id
  cidr_block              = var.public_subnet_cidr_blocks[count.index]
  availability_zone       = data.aws_availability_zones.available.names[count.index]
  map_public_ip_on_launch = "true"

  tags = merge(
    {
      Name        = "Kezbek-Subnet-Public-${count.index}",
      Project     = var.project,
      Owner       = var.owner,
      Environment = var.environment,
      "kubernetes.io/role/elb" = "1" 
    },
    var.tags
  )
}

#################
# Private Subnet
#################
resource "aws_subnet" "private" {
  count      = length(var.private_subnet_cidr_blocks)

  vpc_id     = aws_vpc.main.id
  cidr_block = var.private_subnet_cidr_blocks[count.index]
  availability_zone = data.aws_availability_zones.available.names[count.index]

  tags = merge(
    {
      Name        = "Kezbek-Subnet-Private-${count.index}",
      Project     = var.project,
      Owner       = var.owner,
      Environment = var.environment,
      "kubernetes.io/role/internal-elb" = "1"
    },
    var.tags
  )
}

resource "aws_db_subnet_group" "db_subnet" {
  name       = "db_subnet"
  subnet_ids = aws_subnet.public.*.id
  
  tags = merge(
     {
       Name        = "Kezbek-DB-Subnet",
       Project     = var.project,
       Owner       = var.owner,
       Environment = var.environment
     },
     var.tags
   )
}
