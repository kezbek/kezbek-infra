variable "project" {
  type        = string
  description = "Name of project this VPC is meant to house"
}

variable "owner" {
  type        = string
  description = "Owner of project"
}

variable "environment" {
  type        = string
  description = "Name of environment this VPC is targeting"
}

variable "tags" {
  type = map(string)
  default = {
    ManagedBy = "Terraform"
  }
}

variable "cidr_block" {
  type        = string
  default     = "10.0.0.0/16"
  description = "CIDR block for the VPC"
}

variable "public_subnet_cidr_blocks" {
  type        = list
  default     = ["10.0.2.0/24", "10.0.4.0/24"]
  description = "List of public subnet CIDR blocks"
}

variable "private_subnet_cidr_blocks" {
  type        = list
  default     = ["10.0.1.0/24", "10.0.3.0/24"]
  description = "List of private subnet CIDR blocks"
}

variable "availability_zones" {
  default     = ["us-east-1"]
  type        = list
  description = "List of availability zones"
}
