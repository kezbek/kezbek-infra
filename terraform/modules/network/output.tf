output "sg" {
  value = {
    sg1     = aws_security_group.sg1.id
  }
}

output "vpc" {
  value       = aws_vpc.main
  description = "VPC ID"
}

output "public_subnet_ids" {
  value       = aws_subnet.public
  description = "List of public subnet IDs"
}

output "private_subnet_ids" {
  value       = aws_subnet.private
  description = "List of private subnet IDs"
}

output "db_subnet_ids" {
  value       = aws_db_subnet_group.db_subnet
  description = "List of db subnet IDs"
}
